package com.clearlabs.acgt.unit;

import com.clearlabs.acgt.model.InputData;
import com.clearlabs.acgt.service.DataService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.util.CollectionUtils;

public class DataServiceTest {

    @InjectMocks
    private DataService dataService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void invalidDataThrowsRuntime() {

        String data = "ACGTs";
        RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> dataService.generateData(data));
        Assertions.assertEquals("Invalid data", runtimeException.getMessage());
    }

    @Test
    void validDataReturnsInputData() {

        String data = "ACGTTTAAAACCCCGGG";
        InputData inputData = dataService.generateData(data);
        Assertions.assertEquals(data, inputData.getData());
        Assertions.assertEquals(4, inputData.getG_occurance());
    }

    @Test
    void inputDataListDoesntResetBefore20() {

        for (int i = 0; i < 19; i++) {

            String data = "ACGTTTAAAACCCCGGG";
            InputData inputData = dataService.generateData(data);
        }
        Assertions.assertFalse(CollectionUtils.isEmpty(dataService.inputDataList));
        Assertions.assertEquals(19, dataService.inputDataList.size());
    }

    @Test
    void inputDataListDoesntResetAt20() {

        for (int i=0; i<20; i++) {

            String data = "ACGTTTAAAACCCCGGG";
            InputData inputData = dataService.generateData(data);
        }
        Assertions.assertTrue(CollectionUtils.isEmpty(dataService.inputDataList));
        Assertions.assertEquals(0, dataService.inputDataList.size());
    }

    @Test
    void inputDataListDoesntResetAt20WithSize() {

        for (int i=0; i<25; i++) {

            String data = "ACGTTTAAAACCCCGGG";
            InputData inputData = dataService.generateData(data);
        }
        Assertions.assertFalse(CollectionUtils.isEmpty(dataService.inputDataList));
        Assertions.assertEquals(5, dataService.inputDataList.size());
    }

}

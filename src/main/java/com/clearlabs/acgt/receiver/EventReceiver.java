package com.clearlabs.acgt.receiver;

import com.clearlabs.acgt.model.InputData;
import com.clearlabs.acgt.service.DataService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EventReceiver {

    private final DataService dataService;

    public EventReceiver(DataService dataService) {
        this.dataService = dataService;
    }

    @Bean
    public java.util.function.Consumer<KStream<String, String>> actg() {

        return input ->
                input.foreach((key, value) -> {
                    InputData inputData = dataService.generateData(value);
                    log.debug("data: " + value + "g Occurrence: {}" + inputData.getG_occurance());
                });
    }
}

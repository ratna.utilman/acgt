package com.clearlabs.acgt.model;

import lombok.Data;

@Data
public class InputData {

    private String data;
    private Long g_occurance;

}

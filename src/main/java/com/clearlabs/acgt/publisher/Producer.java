package com.clearlabs.acgt.publisher;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Random;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Component
public class Producer {

    public static final String TOPIC_NAME = "acgt-strings";
    public static final String ACGT = "ACGT";
    private final KafkaTemplate<Integer, String> template;

    @EventListener(ApplicationStartedEvent.class)
    public void generate() {

        Random random = new Random();
        final Flux<Long> interval = Flux.interval(Duration.ofMillis(100));

        final Flux<String> quotes = Flux.fromStream(Stream.generate(() -> generateRandomChars(ACGT, 1000)));


        Flux.zip(interval, quotes)
                .map(it -> template.send(TOPIC_NAME, random.nextInt(42), it.getT2())).blockLast();
    }

    public static String generateRandomChars(String candidateChars, int length) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(candidateChars.charAt(random.nextInt(candidateChars
                    .length())));
        }

        return sb.toString();
    }
}

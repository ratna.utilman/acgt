package com.clearlabs.acgt.service;

import com.clearlabs.acgt.model.InputData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class DataService {

    public static final int RESET_SIZE = 20;
    public List<InputData> inputDataList = new ArrayList<>();

    public InputData generateData(String data) {
        log.debug("inputData for data: {}", data);
        if (validate(data)) {
            log.debug("valid inputData data");
            InputData inputData = new InputData();
            inputData.setData(data);
            inputData.setG_occurance(gOccurance(data));
            inputDataList.add(inputData);
            log.debug("Input data list size: {}", inputDataList.size());
            if (inputDataList.size() >= RESET_SIZE) {
                printData(inputDataList);
                inputDataList = new ArrayList<>();
            }
            return inputData;
        }
        throw new RuntimeException("Invalid data");
    }

    private void printData(List<InputData> inputDataList) {

        print("------------------------------------------------------------------------");
        print(String.format("---------------------Data Analysis for items: %s ------------------------", inputDataList.size()));
        print("------------------------------------------------------------------------");
        for (InputData inputData : inputDataList) {

            print(String.format("Data: %s", inputData.getData()));
            print(String.format("Number of times G appeared: %s", inputData.getG_occurance()));
            print("/////////////////////////////////////////////////////////////////////");
        }
        print("------------------------------------------------------------------------");
        print("----------------------The End-------------------------------------------");
    }

    public void print(String printable) {
        System.out.println(printable);
    }

    private boolean validate(String data) {

        return data.matches("[ACGT]*$");
    }

    private long gOccurance(String data) {

        return data.chars().filter(num -> num == 'G').count();
    }

}

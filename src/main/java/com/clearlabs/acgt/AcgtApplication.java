package com.clearlabs.acgt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcgtApplication {

    public static void main(String[] args) {
        SpringApplication.run(AcgtApplication.class, args);
    }

}

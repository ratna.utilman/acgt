# Service Name

acgt 

## Description

ACGT is data analysis service which streams data from kafka server hosted in confluent cloud.
Currently ACGT itself producing data into kafka topic which is named as acgt-strings.
- [ ] The incoming data is a long stream of characters, in random order, of the 4 letters A, C, G, T.
- [ ] The data cannot be read as a single string, as it arrives in variable lengths and at variable times.
- [ ] Every time a batch of 512 characters has arrived, a function processes that data set. The function should determine how many times the letter G is present in the batch.
- [ ] The component keeps track of the number of batches that have arrived, and the number of times the letter G has been found in each batch.
- [ ] After a certain period of time, the system stops and prints out the result.
- [ ] You can make your own assumptions on how the system should be built.

## Add your files

- [ ] We can start the service by running ```./gradlew bootRun```
- [ ] We can build project by running ```./gradlew clean build```. This will also run unit tests.
- [ ] ```docker build -t .``` will build docker image

### Assumptions
- [ ] Assuming, integration tests would not needed for now, 
 as it would need testContainers and I am not sure if the running environment will be able to 
 download necessary images. 
- [ ] Assuming Data is getting streamed to kafka from cloud. 
- [ ] Assuming system should print the data and report after analysis for every 20 records.
- [ ] Created confluent kafka service in free tier, assuming this is not going to run to use heavy resources.
- [ ] Currently, we analyse the data and report only number of times G is appeared in the string.
- [ ] Assuming, no need to persist data in database.
- [ ] Assuming, no REST API would need to fetch analysed data, that is why didnt develop any API's.
- [ ] Assuming, having producer and consumer in same service is not an issue.
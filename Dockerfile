FROM adoptopenjdk/openjdk11

ADD src/main/resources/application.properties /app/props/application.properties

COPY build/libs/acgt-*-SNAPSHOT.jar /app/libs/acgt.jar

WORKDIR /app

EXPOSE 5000 8080 8778

ENTRYPOINT [ "java", "-jar", "/app/libs/acgt.jar" ]
CMD ["--spring.config.location=/app/props/application.properties" ]

